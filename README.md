## MagicTech #1

**Сервер**

1. AddCraft
2. Advanced Machines
3. Advanced Solar Panel
4. Alfheim
5. Applied Energistics Stuff
6. Applied Energistics
7. Architecture Craft
8. Automagy
9. BiblioCraft
10. Big Reactors
11. Biomes O Plenty
12. Blood Magic
13. Blue Power
14. Botania
15. Build Craft
16. Carpenters Blocks
17. Chat
18. Chicken Chunks
19. Chisel
20. Creative Core
21. Daily Rewards
22. Dragons Radio
23. Dummy Core
24. Electro Magic Tools
25. Ender IO
26. Et Futurum
27. Extra Biomes XL
28. Extra Cells
29. Extra Utilities
30. ExtraTiC
31. FastLeafDecay
32. Forbidden Magic
33. Forestry
34. GlibysVC
35. Gravi Suite
36. Grimoire Of Gaia 3
37. IC2 Combine Armor
38. IC2 Nuclear Control
40. Immersive Engineering
41. Immersive Integration
42. Industrial Craft Experimental
43. Ironchest
44. Magic Bees
45. Pams Harvest Craft
46. Patcher
47. PatcherD
48. PatcherH
49. PatcherW
50. Power Utils
51. Primitive Mobs
52. Shop
53. Storage Drawers
54. Super Solar Panels
55. Tainted Magic
56. TConstruct
57. Thaumcraft
58. Thaumic Bases
59. Thaumic Energistics
60. Thaumic Horizons
61. Thaumic Tinkerer
62. Thermal Dynamics
63. Thermal Expansion
64. Thermal Foundation
65. TiCTooltips
66. Travellers Gear
67. TShop
68. Twilight Forest
69. Wallpaper Craft
70. Witchery
71. Witching Gadgets

**Клиент**

1. [1.7.10]bspkrsCore-universal-6.16
2. Apple Core
3. ARAGO
4. Armor Status HUD
5. AutoRefresh
6. Baubles-1.7.10-1.0.1.10
7. bdlib
8. Better Achievements
9. Better Foliage
10. Better Fps
11. Bugfix
12. Cases
13. Code Chicken Core
14. CodeChickenLib-1.7.10-1.1.3.138-universal
15. CoFH Core
16. Controlling
17. Craft Tweaker
18. Drop Money
19. Dynamic Surroundings
20. Ender Core
21. Ender IO Addons
22. FastCraft
23. Fps Reducer
24. Harvest
25. HoloInventory
26. Inventory Tweaks
27. Journey Map
28. Keybindings
29. Mantle
30. Mod Tweaker
31. Money Viewer
32. Mouse Tweaks
33. NEAT
34. NEI Addons
35. NoMobSpawningOnTrees
36. Not Enough Items
37. Not Enough Resources
38. OnlinePicFrame
39. OptiFine_1.7.10_HD_U_E7
40. p455w0rdslib
41. PacketUnlimiter
42. Parallel MipMap
43. Personalization
44. Qmunity Lib
45. Region Helper
46. RenderPlayerAPI
47. Screen
48. Screenshots
49. Status Effect HUD
50. TabList
51. Thaumcraft Addon
52. Thaumcraft NEI
53. TP Fix
54. Trashslot
55. Treecapitator
56. UpsilonTools
57. Waila
58. Wawla
59. WGViewer
60. World Edit CUI
61. World Tooltips
62. xModBlocker
63. X-Ray
64. EJML-core-0.26
65. commons-dbcp2-2.1.1
66. commons-logging-1.2
67. commons-pool2-2.4.2

## MagicTech #2

**Сервер** 

1. AddCraft
2. Advanced Machines
3. Advanced Solar Panel
4. Alfheim
5. Applied Energistics Stuff
6. Applied Energistics
7. Architecture Craft
8. Automagy
9. BiblioCraft
10. Big Reactors
11. Biomes O Plenty
12. Blood Magic
13. Blue Power
14. Botania
15. Build Craft
16. Carpenters Blocks
17. Chat
18. Chicken Chunks
19. Chisel
20. Creative Core
21. Daily Rewards
22. Draconic Evolution
23. Dragons Radio
24. Dummy Core
25. Electro Magic Tools
26. Ender IO
27. Et Futurum
28. Extra Biomes XL
29. Extra Cells
30. Extra Utilities
31. ExtraTiC
32. FastLeafDecay
33. Forbidden Magic
34. Forestry
35. GlibysVC
36. Gravi Suite
37. Grimoire Of Gaia 3
38. IC2 Combine Armor
40. IC2 Nuclear Control
41. Immersive Engineering
42. Immersive Integration
43. Industrial Craft Experimental
44. Ironchest
45. Magic Bees
46. Pams Harvest Craft
47. Patcher
48. PatcherD
49. PatcherH
50. PatcherW
51. Power Utils
52. Primitive Mobs
53. Shop
54. Storage Drawers
55. Super Solar Panels
56. Tainted Magic
57. TConstruct
58. Thaumcraft
59. Thaumic Bases
60. Thaumic Energistics
61. Thaumic Horizons
62. Thaumic Tinkerer
63. Thermal Dynamics
64. Thermal Expansion
65. Thermal Foundation
66. TiCTooltips
67. Travellers Gear
68. TShop
69. Twilight Forest
70. Wallpaper Craft
71. Witchery
72. Witching Gadgets

**Клиент** 

1. [1.7.10]bspkrsCore-universal-6.16
2. Apple Core
3. ARAGO
4. Armor Status HUD
5. AutoRefresh
6. Baubles-1.7.10-1.0.1.10
7. bdlib
8. Better Achievements
9. Better Foliage
10. Better Fps
11. Bugfix
12. Cases
13. Code Chicken Core
14. CodeChickenLib-1.7.10-1.1.3.138-universal
15. CoFH Core
16. Controlling
17. Craft Tweaker
18. Drop Money
19. Dynamic Surroundings
20. Ender Core
21. Ender IO Addons
22. FastCraft
23. Fps Reducer
24. Harvest
25. HoloInventory
26. Inventory Tweaks
27. Journey Map
28. Keybindings
29. Mantle
30. Mod Tweaker
31. Money Viewer
32. Mouse Tweaks
33. NEAT
34. NEI Addons
35. NoMobSpawningOnTrees
36. Not Enough Items
37. Not Enough Resources
38. OnlinePicFrame
39. OptiFine_1.7.10_HD_U_E7
40. p455w0rdslib
41. PacketUnlimiter
42. Parallel MipMap
43. Personalization
44. Qmunity Lib
45. Region Helper
46. RenderPlayerAPI
47. Screen
48. Screenshots
49. Status Effect HUD
50. TabList
51. Thaumcraft Addon
52. Thaumcraft NEI
53. TP Fix
54. Trashslot
55. Treecapitator
56. UpsilonTools
57. Waila
58. Wawla
59. WGViewer
60. World Edit CUI
61. World Tooltips
62. xModBlocker
63. X-Ray
64. EJML-core-0.26
65. commons-dbcp2-2.1.1
66. commons-logging-1.2
67. commons-pool2-2.4.2
68. Brandons Core




## SkyBlock 

**Сервер **

1. AddCraft 2.Advanced Machines
2. Advanced Solar Panel 
3. Agri Craft 
4. Alfheim 
5. Applied Energistics Stuff 
6. Applied Energistics 
7. Architecture Craft 
8. Automagy 
9. BiblioCraft 
10. Big Reactors 
11. Binnie 
12. Blood Magic 
13. Blue Power 
14. Botania 
15. Build Craft 
16. Carpenters Blocks 
17. Chat 
18. Chicken Chunks 
19. Chisel 
20. Creative Core 
21. Daily Rewards 
22. Draconic Evolution 
23. Dragons Radio 
24. Electro Magic Tools 
25. Ender IO 
26. Ex Astris 
27. Ex Compressum 
28. Ex Nihilo
29. Extra Utilities
30. Extra Cells
31. ExtraTiC
32. FastLeafDecay
33. Forbidden Magic
34. Forestry
35. GlibysVC
36. Gravi Suite
37. IC2 Combine Armor
38. IC2 Nuclear Control
39. Immersive Engineering
40. Immersive Integration
41. Industrial Craft Experimental
42. Magic Bees
43. Magical Crops Deco
44. Magical Crops
45. Mine Factory Reloaded
46. MM Magical Crops
47. Open Blocks
48. Open Mods Lib
49. Pams Harvest Craft
50. Patcher
51. PatcherD
52. PatcherH
53. PatcherW
54. Power Utils
55. Shop
56. Storage Drawers
56. Super Solar Panels
56. Tainted Magic
56. TConstruct
56. Thaumcraft
56. Thaumic Energistics
56. Thaumic Horizons
56. Thaumic Tinkerer
56. Thermal Dynamics
56. Thermal Expansion
56. Thermal Foundation
56. TiCTooltips
56. Travellers Gear
56. TShop
56. Wallpaper Craft
56. Witching Gadgets


**Клиент**

1. [1.7.10]bspkrsCore-universal-6.16.jar
2. Apple Core.jar
3. ARAGO.jar
4. Armor Status HUD.jar
5. AutoRefresh.jar
6. Baubles-1.7.10-1.0.1.10.jar
7. bdlib.jar
8. Better Achievements.jar
9. Better Foliage.jar
10. Better Fps.jar
11. Better Questing.jar
12. Brandons Core.jar
13. Bugfix.jar
14. Cases.jar
15. Code Chicken Core.jar
16. CodeChickenLib-1.7.10-1.1.3.138-universal.jar
17. CoFH Core.jar
18. Controlling.jar
19. Craft Tweaker.jar
20. Drop Money.jar
21. Dynamic Surroundings.jar
22. Ender Core.jar
23. Ender IO Addons.jar
24. FastCraft.jar
25. Fps Reducer.jar
26. Harvest.jar
27. HoloInventory.jar
28. InventoryTweaks-1.59-dev-152.jar
29. Journey Map.jar
30. Keybindings.jar
31. Mantle.jar
32. Mod Tweaker.jar
33. Money Viewer.jar
34. Mouse Tweaks.jar
35. NEI Addons.jar
36. NoMobSpawningOnTrees.jar
37. Not Enough Items.jar
38. Not Enough Resources.jar
39. OnlinePicFrame.jar
40. OptiFine_1.7.10_HD_U_E7.jar
41. p455w0rdslib.jar
42. PacketUnlimiter.jar
43. Parallel MipMap.jar
44. Personalization.jar
45. Qmunity Lib.jar
46. Quest Book.jar
47. Region Helper.jar
48. RenderPlayerAPI.jar
49. Screen.jar
50. Screenshots.jar
51. Standard Expansion.jar
52. Status Effect HUD.jar
53. TabList.jar
54. Thaumcraft Addon.jar
55. Thaumcraft NEI.jar
56. TP Fix.jar
57. Trashslot.jar
58. Treecapitator.jar
59. Waila.jar
60. Wawla.jar
61. WGViewer.jar
62. World Edit CUI.jar
63. World Tooltips.jar
64. xModBlocker.jar
65. X-Ray.jar
66. EJML-core-0.26.jar
67. commons-dbcp2-2.1.1.jar
68. commons-logging-1.2.jar
69. commons-pool2-2.4.2.jar


---

## SpaceTech

**Сервер**

1. AddCraft
2. Advanced Machines
3. Advanced Solar Panel
4. Applied Energistics Stuff
5. Applied Energistics
6. Architecture Craft
7. BiblioCraft
8. Big Reactors
9. Binnie
10. Biomes O Plenty
11. Blue Power
12. Build Craft
13. Carpenters Blocks
14. Chat
15. Chicken Chunks
16. Chisel
17. Creative Core
18. Daily Rewards
19. Dragons Radio
20. Ender IO Addons
21. Ender IO
22. Et Futurum
23. Extra Biomes XL
24. Extra Cells
25. Extra Planets
26. ExtraTiC
27. FastLeafDecay
28. Forestry
29. Galacticraft Core
30. Galacticraft Planets
31. GlibysVC
32. Gravi Suite
33. IC2 Combine Armor
34. IC2 Nuclear Control
35. Immersive Engineering
36. Immersive Integration
37. Industrial Craft Experimental
38. Ironchest
40. Mekanism Generators
41. Mekanism Tools
42. Mekanism
43. Mine Factory Reloaded
44. Pams Harvest Craft
45. Patcher
46. PatcherD
47. PatcherH
48. PatcherW
49. Power Utils
50. Primitive Mobs
51. Shop
52. Starminer
53. Storage Drawers
54. Super Solar Panels
55. TConstruct
56. Thermal Dynamics
57. Thermal Expansion
58. Thermal Foundation
59. TiCTooltips
60. TShop
61. Wallpaper Craft

**Клиент**

1. [1.7.10]bspkrsCore-universal-6.16
2. Apple Core
3. ARAGO
4. Armor Status HUD
5. AutoRefresh
6. bdlib
7. Better Achievements
8. Better Foliage
9. Better Fps
10. Bugfix
11. Cases
12. Code Chicken Core
13. CodeChickenLib-1.7.10-1.1.3.138-universal
14. CoFH Core
15. Controlling
16. Craft Tweaker
17. Drop Money
18. Dynamic Surroundings
19. Ender Core
20. FastCraft
21. ForgeMultipart-1.7.10-1.1.2.331-universal
22. Fps Reducer
23. HoloInventory
24. Inventory Tweaks
25. Journey Map
26. Keybindings
27. Mantle
28. Micdoodle Core
29. Mod Tweaker
30. Money Viewer
31. Mouse Tweaks
32. NEAT
33. NoMobSpawningOnTrees
34. NEI Addons
35. Not Enough Items
36. Not Enough Resources
37. OnlinePicFrame
38. OptiFine_1.7.10_HD_U_E7
39. p455w0rdslib
40. PacketUnlimiter
41. Parallel MipMap
42. Personalization
43. Qmunity Lib
44. Region Helper
45. RenderPlayerAPI
46. Screen
47. Screenshots
48. Status Effect HUD
49. TabList
50. TP Fix
51. Trashslot
52. Treecapitator
53. UpsilonTools
54. Waila
55. Wawla
56. WGViewer
57. World Edit CUI
58. World Tooltips
59. xModBlocker
60. X-Ray
61. EJML-core-0.26
62. commons-dbcp2-2.1.1
63. commons-logging-1.2
64. commons-pool2-2.4.2
---
## MagicRPG

**Сервер**

1. AddCraft
2. Alfheim
3. Animation API
4. Architecture Craft
5. Ars Magica 2
6. Automagy
7. BiblioCraft
8. Blood Magic
9. Botania
10. Carpenters Blocks
11. Chat
12. Chicken Chunks
13. Chisel
14. Creative Core
15. Daily Rewards
16. DivineRPG
17. Dragons Radio
18. Et Futurum
19. Extra Biomes XL
20. FastLeafDecay
21. Forbidden Magic
22. GlibysVC
23. Hardcore Ender Expansion
24. HoloInventory
25. iChunUtil
26. Ironchest
27. OptiFine_1.7.10_HD_U_E7
28. Pams Harvest Craft
29. PatcherD
30. PatcherH
31. PatcherW
32. Shop
33. Storage Drawers
34. Tainted Magic
35. TConstruct
36. Thaumcraft
37. Thaumic Bases
38. Thaumic Horizons
40. Thaumic Tinkerer
41. TiCTooltips
42. Travellers Gear
43. TShop
44. Twilight Forest
45. Wallpaper Craft
46. Witchery
47. Witching Gadgets

**Клиент**

1. [1.7.10]bspkrsCore-universal-6.16
2. ARAGO
3. Armor Status HUD
4. AutoRefresh
5. Baubles-1.7.10-1.0.1.10
6. Better Achievements
7. Better Foliage
8. Better Fps
9. Bugfix
10. Cases
11. Code Chicken Core
12. CodeChickenLib-1.7.10-1.1.3.138-universal
13. Controlling
14. Craft Tweaker
15. Drop Money
16. Dummy Core
17. Dynamic Surroundings
18. FastCraft
19. Fps Reducer.
20. Harvest.
21. Inventory Tweaks.
22. Journey Map.
23. Keybindings.
24. Mantle.
25. Mod Tweaker.
26. Money Viewer.
27. Mouse Tweaks.
28. NEI Addons.
29. NoMobSpawningOnTrees.
30. Not Enough Items.
31. Not Enough Resources.
32. OnlinePicFrame.
33. PacketUnlimiter.
34. Parallel MipMap.
35. Personalization.
36. Qmunity Lib.
37. Region Helper.
38. RenderPlayerAPI.
39. Screen.
40. Screenshots.
41. Status Effect HUD.
42. TabList.
43. Thaumcraft Addon.
44. Thaumcraft NEI.
45. TP Fix.
46. Trashslot.
47. Treecapitator.
48. UpsilonTools.
49. Waila.
50. Wawla.
51. WGViewer.
52. World Edit CUI.
53. World Tooltips.
54. xModBlocker.
55. X-Ray.
